import React from 'react';
import { BrowserRouter } from 'react-router-dom';


import './assets/scss/index.scss';
import LayoutComponent from './components/Layout';

const App = () => (
    <BrowserRouter>
        <LayoutComponent />
    </BrowserRouter>
);

export default App;
