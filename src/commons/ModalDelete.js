import React from 'react';
import { Modal, Button, Input, Typography, message } from "antd";

const { Title } = Typography;

function ModalDelete(props) {
    const { visible, handleDelete, title, handleClose } = props;
    return (
        <Modal
            title={title}
            centered
            visible={visible}
            onCancel={handleClose}
            footer={[
                <Button key="back" onClick={handleClose}>Hủy</Button>,
                <Button key="submit" type="primary" onClick={handleDelete}>Xóa</Button>
            ]}
        >
            <div>
                <Title level={3}>Bạn có chắc chắn muốn xóa ?</Title>
            </div>
        </Modal>
    )
}

export default ModalDelete;