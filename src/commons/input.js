import React from "react";
import { Input } from "antd";

export const inputField = (placeholder) => {
    return <Input placeholder={placeholder} />;
};