import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
    Switch,
    Redirect,
    withRouter,
    Route,
    useHistory,
    useLocation,
} from "react-router-dom";
import { Layout, Menu, Avatar } from "antd";
import {
    DesktopOutlined,
    LogoutOutlined,
    UserOutlined,
    CalendarOutlined,
} from "@ant-design/icons";

import Login from "./auth/login";
import Subjects from "./subjects";
import Users from "./users";
import {
    fetchUserByToken,
    userSelector,
    clearState,
} from "../store/features/Users/UserSlice";
import Events from "./events";
import UserInformation from "./userInformation";
import EventDetail from "./events/EventDetail";
import ForgotPassword from "./auth/login/ForgotPassword";

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

function LayoutComponent() {
    const { isFetching, isError, role, username } = useSelector(userSelector);
    const history = useHistory();
    const location = useLocation();
    const dispatch = useDispatch();
    const [collapsed, setCollapsed] = useState(false);
    const defaultActiveKey = location.pathname.split("/")[1];

    useEffect(() => {
        defaultActiveKey !== "login" &&
            defaultActiveKey !== "forgot-password" &&
            dispatch(
                fetchUserByToken({ token: localStorage.getItem("token") })
            );
    }, []);

    useEffect(() => {
        if (isError) {
            dispatch(clearState());
            history.push("/login");
        }
    }, [isError]);

    const handleLogout = () => {
        localStorage.removeItem("token");
        history.push("/login");
    };

    const handleCollapse = () => {
        setCollapsed(!collapsed);
    };
    return (
        <>
            <Switch>
                <Route path="/login" component={Login} exact/>
                <Route path="/forgot-password" component={ForgotPassword} />
                <Route
                    render={() => {
                        return (
                            <Layout style={{ minHeight: "100vh" }}>
                                <Sider
                                    collapsible
                                    collapsed={collapsed}
                                    onCollapse={handleCollapse}
                                >
                                    {/* <div className="logo" /> */}
                                    <Menu
                                        theme="dark"
                                        defaultSelectedKeys={[
                                            "/" + defaultActiveKey,
                                        ]}
                                        selectedKeys={["/" + defaultActiveKey]}
                                        mode="inline"
                                    >
                                        <Menu.Item
                                            key="/"
                                            className="userInfo"
                                            onClick={() => history.push("/")}
                                        >
                                            <Avatar icon={<UserOutlined />} />
                                            {username}
                                        </Menu.Item>
                                        {role === "ROLE_ADMIN" && (
                                            <Menu.Item
                                                key="/users"
                                                icon={<UserOutlined />}
                                                onClick={() =>
                                                    history.push("/users")
                                                }
                                            >
                                                Quản lí người dùng
                                            </Menu.Item>
                                        )}

                                        <Menu.Item
                                            key="/subjects"
                                            icon={<DesktopOutlined />}
                                            onClick={() =>
                                                history.push("/subjects")
                                            }
                                        >
                                            Quản lí môn học
                                        </Menu.Item>
                                        <Menu.Item
                                            key="/events"
                                            icon={<CalendarOutlined />}
                                            onClick={() =>
                                                history.push("/events")
                                            }
                                        >
                                            Quản lí sự kiện
                                        </Menu.Item>
                                        <Menu.Item
                                            key="4"
                                            icon={<LogoutOutlined />}
                                            onClick={handleLogout}
                                        >
                                            <span>Đăng xuất</span>
                                        </Menu.Item>
                                    </Menu>
                                </Sider>
                                <Layout className="site-layout">
                                    {/* <Header
                                    className="site-layout-background"
                                    style={{ padding: 0 }}
                                /> */}
                                    <Content style={{ margin: "0 16px" }}>
                                        <Switch>
                                            <Route
                                                path="/"
                                                exact
                                                component={UserInformation}
                                            />
                                            <Route
                                                path="/users"
                                                component={Users}
                                            />
                                            <Route
                                                path="/subjects"
                                                component={Subjects}
                                            />
                                            <Switch>
                                                <Route
                                                    path="/events"
                                                    exact
                                                    component={Events}
                                                />
                                                <Route
                                                    path="/events/:id"
                                                    component={EventDetail}
                                                />
                                            </Switch>
                                        </Switch>
                                    </Content>
                                    {/* <Footer style={{ textAlign: "center" }}>
                                    Ant Design ©2018 Created by Ant UED
                                </Footer> */}
                                </Layout>
                            </Layout>
                        );
                    }}
                />
                <Redirect to="/login" />
            </Switch>
        </>
    );
}

export default withRouter(LayoutComponent);
