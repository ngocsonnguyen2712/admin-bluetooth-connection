import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, Input, message, Typography } from "antd";
import { useHistory } from "react-router-dom";

import Messages from "../../../constants/messages";
import { forgotPassword } from "../../../services/users";

const { Title } = Typography;

const schemaValidation = yup.object({
    email: yup.string().required(Messages.ERROR_REQUIRED),
});

function ForgotPassword() {
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm({
        resolver: yupResolver(schemaValidation),
    });
    const history = useHistory();

    const onSubmit = async (data) => {
        const res = await forgotPassword(data);
        if (res) {
            reset();
            message.success(
                "Một email thay đổi mật khẩu đã được gửi về email của bạn. Vui lòng kiểm tra và thực hiện đăng nhập lại"
            );
        }
    };

    return (
        <div className="login">
            <form onSubmit={handleSubmit(onSubmit)} className="login-form">
                <Title level={2} className="title">
                    Quên mật khẩu
                </Title>
                <div className="input-group">
                    <label className="label">Tài khoản</label>
                    <Controller
                        render={({ field }) => (
                            <Input {...field} placeholder="Tài khoản" />
                        )}
                        name="email"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.email?.message}</span>
                </div>
                <Button type="primary" htmlType="submit">
                    Xác nhận
                </Button>
                <Title level={5} className="forgot-password" onClick = {() => history.push('/login')}>
                    Quay lại
                </Title>
            </form>
        </div>
    );
}
export default ForgotPassword;
