import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import { Button, Input, Typography } from "antd";
import { useHistory } from "react-router-dom";

import Messages from '../../../constants/messages';
import { loginUser, userSelector, clearState } from '../../../store/features/Users/UserSlice';

const { Title } = Typography;

const schemaValidation = yup.object({
    email: yup.string().required(Messages.ERROR_REQUIRED),
    password: yup.string().required(Messages.ERROR_REQUIRED),
})

function Login() {
    const { control, handleSubmit, formState: { errors }, reset } = useForm({
        resolver: yupResolver(schemaValidation)
    });
    const history = useHistory();
    const dispatch = useDispatch();
    const { isFetching, isSuccess, isError, errorMessage } = useSelector(
        userSelector
    );

    const onSubmit = data => {
        dispatch(loginUser(data));
    };

    useEffect(() => {
        return () => {
            dispatch(clearState());
        };
    }, []);
    useEffect(() => {
        if (isError) {
            dispatch(clearState());
        }
        if (isSuccess) {
            dispatch(clearState());
            history.push('/');
        }
    }, [isError, isSuccess]);

    return (
        <div className="login">
            <form onSubmit={handleSubmit(onSubmit)} className="login-form">
                <Title level={2} className="title">
                    Hệ thống quản lí điểm danh
                </Title>
                <div className="input-group">
                    <label className="label">Tài khoản</label>
                    <Controller
                        render={({ field }) => (
                            <Input {...field} placeholder="Tài khoản" />
                        )}
                        name="email"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.email?.message}</span>
                </div>
                <div className="input-group">
                    <label className="label">Mật khẩu</label>
                    <Controller
                        render={({ field }) => (
                            <Input
                                {...field}
                                placeholder="Mật khẩu"
                                type="password"
                            />
                        )}
                        name="password"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.password?.message}</span>
                </div>
                <Button type="primary" htmlType="submit">
                    Đăng nhập
                </Button>
                <Title
                    level={5}
                    className="forgot-password"
                    onClick={() => history.push("/forgot-password")}
                >
                    Quên mật khẩu?
                </Title>
            </form>
        </div>
    );
}
export default Login;