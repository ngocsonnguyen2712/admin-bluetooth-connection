import React, { useState, useEffect, useMemo } from "react";
import { useParams } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import {
    Table,
    Space,
    message,
    Button,
    Input,
    Row,
    Col,
    Typography,
} from "antd";
import { DownloadOutlined, SearchOutlined } from "@ant-design/icons";
import moment from "moment";
import Highlighter from "react-highlight-words";
import * as FileSaver from "file-saver";
import * as XLSX from "xlsx";

import {
    getDetailEvent,
    getAllUserAttendanceAnEvent,
    downloadFile,
} from "../../services/events";

const { Title } = Typography;

function EventDetail() {
    const { id } = useParams();
    const [dataForm, setDataForm] = useState();
    const [listUser, setListUser] = useState();
    const [searchText, setSearchText] = useState("");
    const [searchedColumn, setSearchedColumn] = useState("");
    const [loading, setLoading] = useState(false);

    const { control, reset } = useForm({
        defaultValues: useMemo(() => {
            return dataForm;
        }, [dataForm]),
    });

    useEffect(() => {
        const getDataEvent = async () => {
            const res = await getDetailEvent(id);
            if (res) {
                setDataForm({
                    ...res,
                    subjectName: res.subject.name || "",
                    subjectKey: res.subject.subjectKey || "",
                    createdAt: moment(res?.createdAt).format(
                        "DD-MM-YYYY HH:mm:ss"
                    ),
                });
            }
        };
        const getListUser = async () => {
            setLoading(true);
            const res = await getAllUserAttendanceAnEvent(id);
            if (res) {
                setLoading(false);
                setListUser(res.users || []);
            }
        };
        if (id) {
            getDataEvent();
            getListUser();
        }
    }, []);
    useEffect(() => {
        reset(dataForm);
    }, [dataForm]);

    const getColumnSearchProps = (dataIndex, placeholder) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`${placeholder}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ marginBottom: 8, display: "block", width: "100%" }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 100 }}
                    >
                        Tìm kiếm
                    </Button>
                    <Button
                        onClick={() => handleReset(clearFilters)}
                        size="small"
                        style={{ width: 80 }}
                    >
                        Hủy
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#1890ff" : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : "",
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText("");
    };

    const columns = [
        {
            title: "#",
            render: (value, row, index) => {
                return index;
            },
            width: "5%",
        },
        {
            title: "ID",
            dataIndex: "id",
            width: "5%",
        },
        {
            title: "Họ và tên",
            dataIndex: "name",
            width: "20%",
            ...getColumnSearchProps("name", "Họ và tên"),
        },
        {
            title: "Mã số sinh viên",
            width: "10%",
            dataIndex: "mssv",
            ...getColumnSearchProps("mssv", "Mã số sinh viên"),
        },
        {
            title: "Thời gian điểm danh",
            // dataIndex: "subjectName",
            width: "10%",
            render: (_, record) => {
                return moment(record?.createdAt).format("DD-MM-YYYY HH:mm:ss");
            },
        },
    ];

    const exportDataToExcel = async () => {
        const res = await downloadFile(dataForm.id);
        if (res) {
            // const fileName = `Danh_sach_diem_danh_lop ${
            //     dataForm.subjectKey
            // }`;
            const dirtyFileName = res.headers["content-disposition"];
            const regex =
                /filename[^;=\n]*=(?:(\\?['"])(.*?)\1|(?:[^\s]+'.*?')?([^;\n]*))/;
            var fileName = dirtyFileName.match(regex)[3];
            const fileExtension = ".xlsx";
            const data = new Blob([res.data], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            });
            FileSaver.saveAs(data, fileName);
        }
    };

    return (
        <div className="event-detail">
            <div className="form-detail">
                <Title level={3} className="title">
                    Thông tin sự kiện
                </Title>
                <form className="event-info">
                    <Row>
                        <Col span={6}>
                            <div className="input-group">
                                <label className="label">ID</label>
                                <Controller
                                    render={({ field }) => (
                                        <Input {...field} disabled={true} />
                                    )}
                                    name="id"
                                    control={control}
                                />
                            </div>
                        </Col>
                        <Col span={8} offset={1}>
                            <div className="input-group">
                                <label className="label">Mã tín hiệu</label>
                                <Controller
                                    render={({ field }) => (
                                        <Input disabled={true} {...field} />
                                    )}
                                    name="uuid"
                                    control={control}
                                />
                            </div>
                        </Col>
                        <Col span={8} offset={1}>
                            <div className="input-group">
                                <label className="label">Thời gian tạo</label>
                                <Controller
                                    render={({ field }) => (
                                        <Input disabled={true} {...field} />
                                    )}
                                    name="createdAt"
                                    control={control}
                                />
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={11}>
                            <div className="input-group">
                                <label className="label">Tên môn học</label>
                                <Controller
                                    render={({ field }) => (
                                        <Input disabled={true} {...field} />
                                    )}
                                    name="subjectName"
                                    control={control}
                                />
                            </div>
                        </Col>
                        <Col span={11} offset={2}>
                            <div className="input-group">
                                <label className="label">Mã môn học</label>
                                <Controller
                                    render={({ field }) => (
                                        <Input disabled={true} {...field} />
                                    )}
                                    name="subjectKey"
                                    control={control}
                                />
                            </div>
                        </Col>
                    </Row>
                </form>
            </div>
            <div className="list-user-attendance">
                <Table
                    bordered
                    dataSource={listUser}
                    title={() => "Danh sách sinh viên đã điểm danh"}
                    columns={columns}
                    loading={loading}
                    rowClassName="editable-row"
                    pagination={false}
                />
                <Button
                    type="primary"
                    onClick={exportDataToExcel}
                    icon={<DownloadOutlined />}
                    // size="small"
                    style={{ marginTop: "20px" }}
                >
                    Tải xuống danh sách
                </Button>
            </div>
        </div>
    );
}

export default EventDetail;
