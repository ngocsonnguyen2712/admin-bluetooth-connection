import React, { useState, useMemo, useEffect } from "react";
import { Modal, Button, Input, message, Select } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { v4 as uuidv4 } from "uuid";

import { createEvent } from "../../services/events";
import Messages from "../../constants/messages";
import { getAllSubjects } from "../../services/subjects";
import { useSelector } from "react-redux";
import { userSelector } from "../../store/features/Users/UserSlice";

const { Option } = Select;

const schemaValidation = yup.object({
    uuid: yup
        .string()
        .uuid(Messages.NOT_VALID_UUID)
        .required(Messages.ERROR_REQUIRED),
    subjectId: yup.number().required(Messages.ERROR_REQUIRED),
});

function ModalCreateNewEvent(props) {
    const { visible, handleClose, onSuccessful } = props;
    const [subjectsData, setSubjectsData] = useState();
    const { role } = useSelector(userSelector);
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        setError,
        clearErrors,
    } = useForm({
        defaultValues: useMemo(() => {
            return {
                id: 0,
                uuid: "",
                subjectId: null,
            };
        }, [visible]),
        resolver: yupResolver(schemaValidation),
    });

    useEffect(() => {
        const getDataSubjects = async () => {
            const res = await getAllSubjects({ isAdmin: role === "ROLE_ADMIN"});
            if (res) {
                setSubjectsData(res.content);
            }
        };
        visible && getDataSubjects();
    }, [visible]);

    const onSubmit = async (data) => {
        const res = await createEvent(data);
        if (res) {
            message.success("Tạo mới sự kiện thành công");
            handleClose();
            reset();
            onSuccessful();
        }
    };

    const onChangeSubject = (value) => {
        setValue('subjectId', value)
    }

    const onUpdateUUID = () => {
        setValue("uuid", uuidv4());
    };

    const onCloseModal = () => {
        reset();
        handleClose();
        clearErrors();
    };

    return (
        <div className="modal-user">
            <Modal
                centered
                title="Tạo mới sự kiện"
                visible={visible}
                onCancel={onCloseModal}
                footer={[
                    <Button key="back" onClick={onCloseModal}>
                        Hủy
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={handleSubmit(onSubmit)}
                    >
                        Tạo mới
                    </Button>,
                ]}
            >
                <form className="user-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            defaultValue={0}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                    </div>
                    <div className="input-group">
                        <div>
                            <label
                                className="label"
                                style={{
                                    position: "absolute",
                                    marginTop: "10px",
                                }}
                            >
                                Mã tín hiệu
                            </label>
                            <Button
                                type="primary"
                                style={{ float: "right" }}
                                onClick={onUpdateUUID}
                            >
                                Tạo mã mới
                            </Button>
                        </div>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="uuid"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.uuid?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Tên môn học</label>
                        <Select
                            showSearch
                            style={{ width: "100%" }}
                            placeholder="Chọn môn học"
                            optionFilterProp="children"
                            defaultValue=""
                            onChange={onChangeSubject}
                            filterOption={(input, option) =>
                                option.children
                                    .toLowerCase()
                                    .indexOf(input.toLowerCase()) >= 0
                            }
                            filterSort={(optionA, optionB) =>
                                optionA.children
                                    .toLowerCase()
                                    .localeCompare(
                                        optionB.children.toLowerCase()
                                    )
                            }
                        >
                            {subjectsData &&
                                subjectsData.map((item) => (
                                    <Option
                                        key={item.id}
                                    >{`${item.name} - ${item.subjectKey}`}</Option>
                                ))}
                        </Select>
                        <span className="error">{errors.subjectId?.message}</span>
                    </div>
                </form>
            </Modal>
        </div>
    );
}

export default ModalCreateNewEvent;
