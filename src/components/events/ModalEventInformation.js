import React, { useEffect, useState, useMemo } from "react";
import { Modal, Button, Input, message } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { v4 as uuidv4 } from 'uuid';

import { getDetailEvent, updateEvent } from "../../services/events";
import { getAllSubjects } from "../../services/subjects";
import Messages from "../../constants/messages";

const schemaValidation = yup.object({
    uuid: yup
        .string()
        .uuid(Messages.NOT_VALID_UUID)
        .required(Messages.ERROR_REQUIRED),
});

function ModalEventInformation(props) {
    const { visible, onClose, isEdit, id, onSuccessful } = props;
    const [eventData, setEventData] = useState();
    
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(schemaValidation),
        defaultValues: useMemo(() => {
            return eventData;
        }, [eventData, visible]),
    });
    useEffect(() => {
        const getDataEvent = async () => {
            const res = await getDetailEvent(id);
            if (res) {
                setEventData({
                    subjectName: res.subject.name || "",
                    subjectKey: res.subject.subjectKey || "",
                    ...res,
                });
            }
        };
        visible && getDataEvent();
    }, [visible, isEdit]);

    useEffect(() => {
        reset(eventData);
    }, [eventData]);

    const onSubmit = async (data) => {
        const res = await updateEvent(data.id, data);
        if (res) {
            message.success("Cập nhật thông tin thành công");
            onClose();
            onSuccessful();
        }
    };

    const onUpdateUUID = () => {
        setValue('uuid', uuidv4());
    }

    return (
        <div className="modal-event">
            <Modal
                centered
                title="Thông tin môn học"
                visible={visible}
                onCancel={onClose}
                footer={[
                    <Button key="back" onClick={onClose}>
                        {isEdit ? "Hủy" : "Đóng"}
                    </Button>,
                    isEdit && (
                        <Button
                            key="submit"
                            type="primary"
                            onClick={handleSubmit(onSubmit)}
                        >
                            Cập nhật
                        </Button>
                    ),
                ]}
            >
                <form className="event-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.email?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <div>
                            <label className="label" style={{ position: 'absolute', marginTop: '10px'}}>Mã tín hiệu</label>
                            <Button type="primary" style={{ float: "right" }} onClick={onUpdateUUID}>Tạo mã mới</Button>
                        </div>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="uuid"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.uuid?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Tên môn học</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={true} {...field} />
                            )}
                            name="subjectName"
                            control={control}
                            rules={{ required: true }}
                        />
                    </div>

                    {/* } */}
                    <div className="input-group">
                        <label className="label">Mã môn học</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={true} {...field} />
                            )}
                            name="subjectKey"
                            control={control}
                            rules={{ required: true }}
                        />
                    </div>
                </form>
            </Modal>
        </div>
    );
}
export default ModalEventInformation;
