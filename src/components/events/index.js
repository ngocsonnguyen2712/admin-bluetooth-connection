import React, { useEffect, useState } from 'react';
import { Table, Space, message, Button, Input } from "antd";
import {
    EyeOutlined,
    EditOutlined,
    DeleteOutlined,
    SearchOutlined,
} from "@ant-design/icons";
import moment from 'moment';
import Highlighter from "react-highlight-words";
import { useHistory } from 'react-router-dom';

import { deleteEvent, getAllEvents } from '../../services/events';
import ModalEventInformation from './ModalEventInformation';
import ModalDelete from '../../commons/ModalDelete';
import ModalCreateNewEvent from './ModalCreateNewEvent';
import { useSelector } from 'react-redux';
import { userSelector } from '../../store/features/Users/UserSlice';

function Events() {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [id, setId] = useState(null);
    const [searchText, setSearchText] = useState("");
    const [searchedColumn, setSearchedColumn] = useState("");
    const { role, isFetching } = useSelector(userSelector);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10
    });
    const history = useHistory();
    const fetchData = async (pagination) => {
        setLoading(true);
        const res = await getAllEvents({
            page: pagination.current - 1,
            size: pagination.pageSize,
            isAdmin: role === "ROLE_ADMIN",
        });
        if (res) {
            setLoading(false);
            // setData(res.content);
            const dataDisplay = res.content;
            dataDisplay.forEach((item) => {
                item.subjectName = item.subject.name;
                item.subjectKey = item.subject.subjectKey;
                item.createdAt = moment(item?.createdAt).format('DD-MM-YYYY HH:mm:ss')
            })
            setData(dataDisplay)
            setPagination({
                ...pagination,
                current: res.currentPage + 1,
                total: res.totalItems
            })
        }
    }

    useEffect(() => {
        !isFetching && role && fetchData(pagination);
    }, [role])

    const handleViewDetailEvent = (record) => {
        history.push(`/events/${record.id}`);
    }
    const handleEditEvent = (record) => {
        setIsEdit(true);
        setVisible(true);
        setId(record.id);
    }
    const handleDeleteEvent = async () => {
        const res = await deleteEvent(id);
        if (res) {
            message.success('Xóa sự kiện thành công');
            setVisibleDelete(false);
            fetchData(pagination);
        } else {
            message.success('Có lỗi bất ngờ xảy ra. Vui lòng thử lại trong giây lát !');
        }
    }

    const handleCloseModal = () => {
        setVisible(false);
        setIsEdit(false);
        setId(null);
    }

    const getColumnSearchProps = (dataIndex, placeholder) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`${placeholder}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ marginBottom: 8, display: "block", width: "100%" }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 100 }}
                    >
                        Tìm kiếm
                    </Button>
                    <Button
                        onClick={() => handleReset(clearFilters)}
                        size="small"
                        style={{ width: 80 }}
                    >
                        Hủy
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#1890ff" : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : "",
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText("");
    };

    const columns = [
        {
            title: "#",
            render: (value, row, index) => {
                return (
                    index + (pagination.current - 1) * pagination.pageSize + 1
                );
            },
            width: "5%",
        },
        {
            title: "ID",
            dataIndex: "id",
            width: "5%",
        },
        {
            title: "Mã tín hiệu",
            dataIndex: "uuid",
            width: "20%",
            ...getColumnSearchProps("uuid", "Mã tín hiệu"),
        },
        {
            title: "Thời gian tạo",
            width: "10%",
            dataIndex: "createdAt",
            ...getColumnSearchProps("createdAt", "Thời gian"),
        },
        {
            title: "Tên môn học",
            dataIndex: "subjectName",
            width: "10%",
            ...getColumnSearchProps("subjectName", "Tên môn học"),
        },
        {
            title: "Mã môn học",
            width: "10%",
            dataIndex: "subjectKey",
            ...getColumnSearchProps("subjectKey", "Mã môn học"),
        },
        {
            title: () => (
                <Button
                    type="primary"
                    danger
                    onClick={() => setOpenModalAdd(true)}
                >
                    Tạo mới
                </Button>
            ),
            width: "10%",
            render: (_, record) => (
                <>
                    <span
                        style={{ marginRight: "20px", cursor: "pointer" }}
                        onClick={() => handleViewDetailEvent(record)}
                    >
                        <EyeOutlined style={{ fontSize: 23 }} />
                    </span>
                    <span
                        style={{ marginRight: "20px", cursor: "pointer" }}
                        onClick={() => handleEditEvent(record)}
                    >
                        <EditOutlined style={{ fontSize: 23 }} />
                    </span>
                    <span
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                            setId(record.id);
                            setVisibleDelete(true);
                        }}
                    >
                        <DeleteOutlined style={{ fontSize: 23 }} />
                    </span>
                </>
            ),
        },
    ];
    return (
        <div className="users-screen">
            <Table
                bordered
                dataSource={data}
                title={() => 'Danh sách sự kiện'}
                columns={columns}
                loading={loading}
                rowClassName="editable-row"
                pagination={{
                    ...pagination,
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ['10', '20', '30', '50', '100']
                }}
                onChange={fetchData}

            />
            <ModalEventInformation
                visible={visible}
                isEdit={isEdit}
                id={id}
                onClose={handleCloseModal}
                onSuccessful={() => fetchData(pagination)}
            />
            <ModalDelete
                visible={visibleDelete}
                handleDelete={handleDeleteEvent}
                title="Xóa sự kiện"
                handleClose={() => setVisibleDelete(false)}
            />
            <ModalCreateNewEvent
                visible={openModalAdd}
                onSuccessful={() => fetchData(pagination)}
                handleClose={() => setOpenModalAdd(false)}
            />
        </div>
    )
}
export default Events;