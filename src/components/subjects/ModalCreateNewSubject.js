import React, { useState, useMemo, useEffect } from "react";
import { Modal, Button, Input, message, Select } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";

import { createSubject } from '../../services/subjects';
import Messages from "../../constants/messages";
import { getAllUsersRoleTeacher } from "../../services/users";

const { Option } = Select;

const schemaValidation = yup.object({
    subjectKey: yup.string().required(Messages.ERROR_REQUIRED),
    name: yup.string().required(Messages.ERROR_REQUIRED),
})

function ModalCreateNewSubject(props) {
    const { visible, handleClose, onSuccessful } = props;
    const [listTeacher, setListTeacher] = useState();
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        setError,
        clearErrors,
    } = useForm({
        defaultValues: useMemo(() => {
            return {
                id: 0,
                subjectKey: "",
                name: "",
                semester: "",
                userId: 0,
                teacher:''
            };
        }, [visible]),
        resolver: yupResolver(schemaValidation),
    });

     useEffect(() => {
         const getListTeacher = async () => {
             const res = await getAllUsersRoleTeacher();
             if (res) {
                 setListTeacher(res.rows || []);
             }
         };
         visible && getListTeacher();
     }, [visible]);

    const onSubmit = async (data) => {
        const res = await createSubject(data);
        if (res) {
            message.success('Tạo mới môn học thành công');
            handleClose();
            reset();
            onSuccessful();
        }
    }

    const onChangeTeacher = (value) => {
        setValue("userId", parseInt(value));
        setValue("teacher", value);
    };

    const onCloseModal = () => {
        reset();
        handleClose();
        clearErrors();
    }

    return (
        <div className="modal-user">
            <Modal
                centered
                title="Tạo mới môn học"
                visible={visible}
                onCancel={onCloseModal}
                footer={[
                    <Button key="back" onClick={onCloseModal}>
                        Hủy
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={handleSubmit(onSubmit)}
                    >
                        Tạo mới
                    </Button>,
                ]}
            >
                <form className="user-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            defaultValue={0}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.email?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Mã môn học</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="subjectKey"
                            control={control}
                            rules={{ required: true }}
                        />
                        <span className="error">
                            {errors.subjectKey?.message}
                        </span>
                    </div>
                    <div className="input-group">
                        <label className="label">Tên môn học</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="name"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.name?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Giảng viên</label>
                        <Controller
                            render={({ field }) => (
                                <Select
                                    {...field}
                                    // labelInValue
                                    showSearch
                                    style={{ width: "100%" }}
                                    placeholder="Chọn giảng viên"
                                    optionFilterProp="children"
                                    defaultValue={""}
                                    onChange={onChangeTeacher}
                                    filterOption={(input, option) =>
                                        option.children
                                            .toLowerCase()
                                            .indexOf(input.toLowerCase()) >= 0
                                    }
                                    filterSort={(optionA, optionB) =>
                                        optionA.children
                                            .toLowerCase()
                                            .localeCompare(
                                                optionB.children.toLowerCase()
                                            )
                                    }
                                >
                                    {listTeacher &&
                                        listTeacher.map((item) => (
                                            <Option
                                                value={item.id}
                                                key={item.name}
                                            >{`${item.name}`}</Option>
                                        ))}
                                </Select>
                            )}
                            name="teacher"
                            control={control}
                        />
                    </div>
                    <div className="input-group">
                        <label className="label">Học kỳ</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="semester"
                            control={control}
                            defaultValue=""
                        />
                    </div>
                </form>
            </Modal>
        </div>
    );
}

export default ModalCreateNewSubject;