import React, { useEffect, useState, useMemo } from "react";
import { Modal, Button, Input, Select, message } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import moment from "moment";

import { getDetailSubject, updateSubject } from "../../services/subjects";
import Messages from "../../constants/messages";
import { getAllUsersRoleTeacher } from "../../services/users";

const { Option } = Select;

const schemaValidation = yup.object({
    name: yup.string().required(Messages.ERROR_REQUIRED),
    userId: yup.string().required(Messages.ERROR_REQUIRED),
});

function ModalSubjectInformation(props) {
    const { visible, onClose, isEdit, id, onSuccessful, defaultTeacher } =
        props;
    const [subjectData, setSubjectData] = useState();
    const [listTeacher, setListTeacher] = useState();
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(schemaValidation),
        defaultValues: useMemo(() => {
            return subjectData;
        }, [subjectData, visible]),
    });
    useEffect(() => {
        const getDataSubject = async () => {
            const res = await getDetailSubject(id);
            if (res) {
                setSubjectData({ teacher: res?.user?.name || "", ...res });
            }
        };
        const getListTeacher = async () => {
            const res = await getAllUsersRoleTeacher();
            if (res) {
                setListTeacher(res.rows || []);
            }
        };
        visible && getDataSubject();
        visible && isEdit && getListTeacher();
    }, [visible, isEdit]);

    useEffect(() => {
        reset(subjectData);
    }, [subjectData]);

    const onSubmit = async (data) => {
        const res = await updateSubject(data.id, data);
        if (res) {
            message.success("Cập nhật thông tin thành công");
            reset();
            onClose();
            onSuccessful();
        }
    };

    const onChangeTeacher = (value) => {
        setValue("userId", parseInt(value));
        setValue("teacher", value);
    };

    const handleClose = () => {
        reset();
        onClose();
    }

    return (
        <div className="modal-subject">
            <Modal
                centered
                title="Thông tin môn học"
                visible={visible}
                onCancel={handleClose}
                footer={[
                    <Button key="back" onClick={handleClose}>
                        {isEdit ? "Hủy" : "Đóng"}
                    </Button>,
                    isEdit && (
                        <Button
                            key="submit"
                            type="primary"
                            onClick={handleSubmit(onSubmit)}
                        >
                            Cập nhật
                        </Button>
                    ),
                ]}
            >
                <form className="subject-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.email?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Mã môn học</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="subjectKey"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">
                            {errors.subjectKey?.message}
                        </span>
                    </div>
                    <div className="input-group">
                        <label className="label">Tên môn học</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="name"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.name?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Giảng viên</label>
                        {isEdit ? (
                            <Controller
                                render={({ field }) => (
                                    <Select
                                        {...field}
                                        showSearch
                                        style={{ width: "100%" }}
                                        placeholder="Chọn giảng viên"
                                        optionFilterProp="children"
                                        defaultValue={defaultTeacher}
                                        onChange={onChangeTeacher}
                                        filterOption={(input, option) =>
                                            option.children
                                                .toLowerCase()
                                                .indexOf(input.toLowerCase()) >=
                                            0
                                        }
                                        filterSort={(optionA, optionB) =>
                                            optionA.children
                                                .toLowerCase()
                                                .localeCompare(
                                                    optionB.children.toLowerCase()
                                                )
                                        }
                                    >
                                        {listTeacher &&
                                            listTeacher.map((item) => (
                                                <Option
                                                    key={item.id}
                                                >{`${item.name}`}</Option>
                                            ))}
                                    </Select>
                                )}
                                name="teacher"
                                control={control}
                            />
                        ) : (
                            <Controller
                                render={({ field }) => (
                                    <Input {...field} disabled={true} />
                                )}
                                name="teacher"
                                control={control}
                            />
                        )}
                        <span className="error">
                            {errors.subjectId?.message}
                        </span>
                    </div>
                    <div className="input-group">
                        <label className="label">Học kì</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="semester"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.password?.message}</span> */}
                    </div>
                </form>
            </Modal>
        </div>
    );
}
export default ModalSubjectInformation;
