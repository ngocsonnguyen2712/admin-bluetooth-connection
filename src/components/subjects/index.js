import React, { useEffect, useState } from "react";
import { Table, message, Button, Input, Space } from "antd";
import {
    EyeOutlined,
    EditOutlined,
    DeleteOutlined,
    SearchOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { useSelector } from "react-redux";

import { deleteSubject, getAllSubjects } from "../../services/subjects";
import ModalUSubjectInformation from "./ModalSubjectInformation";
import ModalDelete from "../../commons/ModalDelete";
import ModalCreateNewSubject from "./ModalCreateNewSubject";
import { userSelector } from "../../store/features/Users/UserSlice";

function Subjects() {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [id, setId] = useState(null);
    const [defaultTeacher, setDefaultTeacher] = useState();
    const [searchText, setSearchText] = useState("");
    const [searchedColumn, setSearchedColumn] = useState("");
    const { role, isFetching } = useSelector(userSelector);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
    });
    const fetchData = async (pagination) => {
        setLoading(true);
        const res = await getAllSubjects({
            page: pagination.current - 1,
            size: pagination.pageSize,
            isAdmin: role === "ROLE_ADMIN",
        });
        if (res) {
            setLoading(false);
            setData(res.content);
            setPagination({
                ...pagination,
                current: res.currentPage + 1,
                total: res.totalItems,
            });
        }
    };

    useEffect(() => {
        !isFetching && role && fetchData(pagination);
    }, [role]);

    const handleViewDetailSubject = (record) => {
        setVisible(true);
        setId(record.id);
    };
    const handleEditSubject = (record) => {
        setIsEdit(true);
        setDefaultTeacher(record?.user?.name || "");
        setVisible(true);
        setId(record.id);
    };
    const handleDeleteSubject = async () => {
        const res = await deleteSubject(id);
        if (res) {
            message.success("Xóa môn học thành công");
            setVisibleDelete(false);
            fetchData(pagination);
        } else {
            message.success(
                "Có lỗi bất ngờ xảy ra. Vui lòng thử lại trong giây lát !"
            );
        }
    };

    const handleCloseModal = () => {
        setVisible(false);
        setIsEdit(false);
        setId(null);
    };

    const getColumnSearchProps = (dataIndex, placeholder) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`${placeholder}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ marginBottom: 8, display: "block", width: "100%" }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 100 }}
                    >
                        Tìm kiếm
                    </Button>
                    <Button
                        onClick={() => handleReset(clearFilters)}
                        size="small"
                        style={{ width: 80 }}
                    >
                        Hủy
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#1890ff" : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : "",

        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText("");
    };

    const columns = [
        {
            title: "#",
            render: (value, row, index) => {
                return (
                    index + (pagination.current - 1) * pagination.pageSize + 1
                );
            },
            width: "5%",
        },
        {
            title: "ID",
            dataIndex: "id",
            width: "5%",
        },
        {
            title: "Mã môn học",
            dataIndex: "subjectKey",
            width: "7%",
            ...getColumnSearchProps("subjectKey", "Mã môn học"),
        },
        {
            title: "Tên môn học",
            dataIndex: "name",
            width: "12%",
            ...getColumnSearchProps("name", "Tên môn học"),
        },
        {
            title: "Giảng viên",
            width: "12%",
            // dataIndex: "teacher",
            render: (_, record) => {
                return record?.user?.name || "";
            },
        },
        {
            title: "Học kỳ",
            dataIndex: "semester",
            width: "10%",
        },
        {
            title: () =>
                role == "ROLE_ADMIN" && (
                    <Button
                        type="primary"
                        danger
                        onClick={() => setOpenModalAdd(true)}
                    >
                        Tạo mới
                    </Button>
                ),
            width: role === "ROLE_ADMIN" ? "10%" : '5%',
            render: (_, record) => (
                <>
                    <span
                        style={{ marginRight: "30px", cursor: "pointer" }}
                        onClick={() => handleViewDetailSubject(record)}
                    >
                        <EyeOutlined style={{ fontSize: 23 }} />
                    </span>
                    {role === "ROLE_ADMIN" && (
                        <span
                            style={{ marginRight: "30px", cursor: "pointer" }}
                            onClick={() => handleEditSubject(record)}
                        >
                            <EditOutlined style={{ fontSize: 23 }} />
                        </span>
                    )}
                    {role === "ROLE_ADMIN" && (
                        <span
                            style={{ marginRight: "20px", cursor: "pointer" }}
                            onClick={() => {
                                setId(record.id);
                                setVisibleDelete(true);
                            }}
                        >
                            <DeleteOutlined style={{ fontSize: 23 }} />
                        </span>
                    )}
                </>
            ),
        },
    ];
    return (
        <div className="users-screen">
            <Table
                bordered
                dataSource={data}
                title={() => "Danh sách môn học"}
                columns={columns}
                loading={loading}
                rowClassName="editable-row"
                pagination={{
                    ...pagination,
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "30", "50", "100"],
                }}
                onChange={fetchData}
            />
            <ModalUSubjectInformation
                visible={visible}
                isEdit={isEdit}
                id={id}
                onClose={handleCloseModal}
                defaultTeacher={defaultTeacher}
                onSuccessful={() => fetchData(pagination)}
            />
            <ModalDelete
                visible={visibleDelete}
                handleDelete={handleDeleteSubject}
                title="Xóa môn học"
                handleClose={() => setVisibleDelete(false)}
            />
            <ModalCreateNewSubject
                visible={openModalAdd}
                onSuccessful={() => fetchData(pagination)}
                handleClose={() => setOpenModalAdd(false)}
            />
        </div>
    );
}
export default Subjects;
