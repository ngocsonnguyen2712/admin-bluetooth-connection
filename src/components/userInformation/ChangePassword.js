import React, { useState, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, DatePicker, Input, message, Typography } from "antd";
import { useHistory } from "react-router-dom";
import moment from "moment";

import Messages from "../../constants/messages";
import {
    loginUser,
    userSelector,
    clearState,
} from "../../store/features/Users/UserSlice";
import { changePassword } from "../../services/users";

const { Title } = Typography;

const schemaValidation = yup.object({
    oldPassword: yup.string().required(Messages.ERROR_REQUIRED),
    newPassword: yup
        .string()
        .min(6, Messages.ERROR_PASSWORD)
        .required(Messages.ERROR_REQUIRED),
    confirmPassword: yup
        .string()
        .required(Messages.ERROR_REQUIRED)
        .when("newPassword", {
            is: (value) => value?.length >= 6,
            then: yup
                .string()
                .oneOf(
                    [yup.ref("newPassword")],
                    Messages.ERROR_PASSWORD_MUST_MATCH
                ),
        }),
});

function ChangePassword() {
    const { id } = useSelector(userSelector);

    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
    } = useForm({
        resolver: yupResolver(schemaValidation),
    });

    const onSubmit = async (data) => {
        const res = await changePassword(data);
        if (res) {
            message.success("Đổi mật khẩu thành công");
            reset();
        }
    };

    return (
        <div>
            <form
                // onSubmit={handleSubmit(onSubmit)}
                className="change-password-form"
            >
                <Title level={2} className="title">
                    Đổi mật khẩu
                </Title>
                <div className="input-group">
                    <label className="label">Mật khẩu cũ</label>
                    <Controller
                        render={({ field }) => (
                            <Input
                                {...field}
                                type="password"
                                placeholder="Mật khẩu cũ"
                            />
                        )}
                        name="oldPassword"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.oldPassword?.message}</span>
                </div>
                <div className="input-group">
                    <label className="label">Mật khẩu mới</label>
                    <Controller
                        render={({ field }) => (
                            <Input
                                {...field}
                                type="password"
                                placeholder="Mật khẩu mới"
                            />
                        )}
                        name="newPassword"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.newPassword?.message}</span>
                </div>
                <div className="input-group">
                    <label className="label">Xác nhận lại mật khẩu</label>
                    <Controller
                        render={({ field }) => (
                            <Input
                                {...field}
                                placeholder="Xác nhận lại mật khẩu"
                                type="password"
                            />
                        )}
                        name="confirmPassword"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">
                        {errors.confirmPassword?.message}
                    </span>
                </div>
                <Button type="primary" onClick={handleSubmit(onSubmit)}>
                    Cập nhật
                </Button>
            </form>
        </div>
    );
}

export default ChangePassword;
