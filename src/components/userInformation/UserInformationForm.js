import React, { useState, useEffect, useMemo } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, DatePicker, Input, message, Typography } from "antd";
import { useHistory } from "react-router-dom";
import moment from "moment";

import Messages from "../../constants/messages";
import {
    loginUser,
    userSelector,
    clearState,
} from "../../store/features/Users/UserSlice";
import { getDetailUser, updateUser } from "../../services/users";

const { Title } = Typography;
const schemaValidation = yup.object({
    name: yup.string().required(Messages.ERROR_REQUIRED),
});

function UserInformationForm() {
    const [userData, setUserData] = useState();
    const { id } = useSelector(userSelector);

    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
    } = useForm({
        defaultValues: useMemo(() => {
            return userData;
        }, [userData, id]),
        resolver: yupResolver(schemaValidation),
    });
    const [date, setDate] = useState();

    useEffect(() => {
        const getDataUser = async () => {
            const res = await getDetailUser(id);
            if (res) {
                setUserData(res);
            }
        };
        id && getDataUser();
    }, [id]);

    useEffect(() => {
        reset(userData);
    }, [userData]);

    const onSubmit = async (data) => {
        const res = await updateUser(data.id, data);
        if (res) {
            message.success("Cập nhật thông tin thành công");
        }
    };

    const onChangeDate = (date, dateString) => {
        setValue("dateOfBirth", dateString);
        setDate(dateString);
    };

    return (
        <div>
            <form className="user-information-form">
                <Title level={2} className="title">
                    Thông tin cá nhân
                </Title>
                <div className="input-group">
                    <label className="label">Họ và tên</label>
                    <Controller
                        render={({ field }) => (
                            <Input {...field} placeholder="Họ và tên" />
                        )}
                        name="name"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                    <span className="error">{errors.name?.message}</span>
                </div>
                <div className="input-group">
                    <label className="label">Email</label>
                    <Controller
                        render={({ field }) => (
                            <Input
                                {...field}
                                disabled={true}
                                placeholder="email"
                            />
                        )}
                        name="email"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                </div>
                <div className="input-group">
                    <label className="label">Địa chỉ</label>
                    <Controller
                        render={({ field }) => (
                            <Input {...field} placeholder="Địa chỉ" />
                        )}
                        name="address"
                        control={control}
                        defaultValue=""
                        rules={{ required: true }}
                    />
                </div>
                <div className="input-group">
                    <label className="label">Ngày sinh</label>
                    <Controller
                        render={({ field }) => (
                            <DatePicker
                                placeholder={"Ngày tháng"}
                                style={{ width: "100%" }}
                                onChange={onChangeDate}
                                format="DD-MM-YYYY"
                                showToday={false}
                                value={date ? moment(date, "DD-MM-YYYY") : ""}
                            />
                        )}
                        name="dateOfBirth"
                        control={control}
                        defaultValue=""
                    />
                </div>
                <Button
                    key="submit"
                    type="primary"
                    onClick={handleSubmit(onSubmit)}
                >
                    Cập nhật
                </Button>
            </form>
        </div>
    );
}
export default UserInformationForm;
