import React from "react";
import { Row, Col } from "antd";
import UserInformationForm from "./UserInformationForm";
import ChangePassword from "./ChangePassword";

function UserInformation() {
    return (
        <Row className="row user-information">
            <Col span={14} className="user-col" style={{background: "#ffffff"}}>
                <UserInformationForm />
            </Col>
            <Col
                span={9}
                offset={1}
                className="user-col"
                 style={{background: "#ffffff"}}
            >
                <ChangePassword />
            </Col>
        </Row>
    );
}

export default UserInformation;
