import React, { useState, useMemo } from "react";
import { Modal, Button, Input, DatePicker, message, Select } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { cloneDeep } from "lodash";
import moment from "moment";

import { createUser, getDetailUser, updateUser } from "../../services/users";
import Messages from "../../constants/messages";

const { Option } = Select;

const schemaValidation = yup.object({
    name: yup.string().required(Messages.ERROR_REQUIRED),
    email: yup
        .string()
        .email(Messages.NOT_VALID_EMAIL)
        .required(Messages.ERROR_REQUIRED),
    roles: yup.string().required(Messages.ERROR_REQUIRED),
});

function ModalCreateNewUser(props) {
    const { visible, handleClose, onSuccessful } = props;
    const [roles, setRoles] = useState();
    const [date, setDate] = useState();
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        setError,
        clearErrors,
        getValues,
    } = useForm({
        defaultValues: useMemo(() => {
            return {
                address: "",
                dateOfBirth: "",
                email: "",
                id: 0,
                name: "",
                roles: "",
            };
        }, [visible]),
        resolver: yupResolver(schemaValidation),
    });

    const onChangeDate = (date, dateString) => {
        setValue("dateOfBirth", dateString);
        setDate(dateString);
    };

    const onChangeRole = (value) => {
        setValue("roles", value);
        clearErrors("mssv");
        setRoles(value);
    };

    const onSubmit = async (data) => {
        if (data.roles === "student" && !data.mssv) {
            setError("mssv", {
                type: "focus",
                message: Messages.ERROR_REQUIRED,
            });
            return;
        }
        const dataSend = cloneDeep(data);
        dataSend.roles = [data.roles];
        const res = await createUser(dataSend);
        if (res) {
            message.success("Tạo mới người dùng thành công");
            reset();
            setRoles(null);
            setValue("dateOfBirth", "");
            setDate("");
            handleClose();
            onSuccessful();
        }
    };

    const onCloseModal = () => {
        reset();
        setDate("");
        handleClose();
        clearErrors();
    };

    return (
        <div className="modal-user">
            <Modal
                centered
                title="Thông tin người dùng"
                visible={visible}
                onCancel={onCloseModal}
                footer={[
                    <Button key="back" onClick={onCloseModal}>
                        Hủy
                    </Button>,
                    <Button
                        key="submit"
                        type="primary"
                        onClick={handleSubmit(onSubmit)}
                    >
                        Tạo mới
                    </Button>,
                ]}
            >
                <form className="user-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            defaultValue={0}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.email?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Họ và tên</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="name"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.name?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Email</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="email"
                            control={control}
                            rules={{ required: true }}
                        />
                        <span className="error">{errors.email?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Ngày sinh</label>
                        <Controller
                            render={({ field }) => (
                                <DatePicker
                                    placeholder={"Ngày tháng"}
                                    style={{ width: "100%" }}
                                    onChange={onChangeDate}
                                    format="DD-MM-YYYY"
                                    showToday={false}
                                    value={
                                        date ? moment(date, "DD-MM-YYYY") : ""
                                    }
                                />
                            )}
                            name="dateOfBirth"
                            control={control}
                            defaultValue=""
                        />
                    </div>
                    <div className="input-group">
                        <label className="label">Địa chỉ</label>
                        <Controller
                            render={({ field }) => <Input {...field} />}
                            name="address"
                            control={control}
                            defaultValue=""
                        />
                    </div>
                    <div className="input-group">
                        <label className="label">Chức danh</label>
                        <Controller
                            render={({ field }) => (
                                <Select
                                    { ...field }
                                    onChange={onChangeRole}
                                    style={{ width: "100%" }}
                                >
                                    <Option key={"admin"}>Admin</Option>
                                    <Option key={"teacher"}>Giảng viên</Option>
                                    <Option key={"student"}>Sinh viên</Option>
                                </Select>
                            )}
                            name="roles"
                            control={control}
                            // defaultValue={userData?.roles[0].name}
                        />
                        <span className="error">{errors.roles?.message}</span>
                    </div>
                    {roles === "student" && (
                        <div className="input-group">
                            <label className="label">Mã số sinh viên</label>
                            <Controller
                                render={({ field }) => <Input {...field} />}
                                name="mssv"
                                control={control}
                                defaultValue=""
                                rules={{ required: true }}
                            />
                            <span className="error">
                                {errors.mssv?.message}
                            </span>
                        </div>
                    )}
                </form>
            </Modal>
        </div>
    );
}

export default ModalCreateNewUser;
