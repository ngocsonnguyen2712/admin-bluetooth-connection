import React, { useEffect, useState, useMemo } from "react";
import { Modal, Button, Input, DatePicker, message } from "antd";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import moment from 'moment';

import { getDetailUser, updateUser } from '../../services/users';
import Messages from "../../constants/messages";

const schemaValidation = yup.object({
    name: yup.string().required(Messages.ERROR_REQUIRED),
})

function ModalUserInformation(props) {
    const { visible, onClose, isEdit, id, onSuccessful } = props;
    const [userData, setUserData] = useState();
    const [date, setDate] = useState();
    const [userRole, setUserRole] = useState('')
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
        getValues,
    } = useForm({
        resolver: yupResolver(schemaValidation),
        defaultValues: useMemo(() => {
            return userData;
        }, [userData, visible])
    });
    useEffect(() => {
        const getDataUser = async () => {
            const res = await getDetailUser(id);
            if (res) {
                setUserData(res);
            }
        }
        visible && getDataUser();
    }, [visible]);

    useEffect(() => {
        reset(userData);
        setDate(userData?.dateOfBirth)
    }, [userData]);

    const onChangeDate = (date, dateString) => {
        setValue('dateOfBirth', dateString);
        setDate(dateString);
    }

    const onSubmit = async (data) => {
        const res = await updateUser(data.id, data);
        if(res) {
            message.success('Cập nhật thông tin thành công');
            onClose();
            onSuccessful();
        }
    }
    const getUserRole = () => {
        let role = '';
        switch (userData?.roles[0].name) {
            case "student":
                role = "Sinh viên";
                break;
            case "teacher":
                 role = "Giảng viên";
                 break;
            case "admin":
                 role = "Admin";
                 break;
            default:
                break
        }
        setUserRole(role);
    }

    useEffect(() => {
        getUserRole();
    }, [userData])
    return (
        <div className="modal-user">
            <Modal
                centered
                title="Thông tin người dùng"
                visible={visible}
                onCancel={onClose}
                footer={[
                    <Button key="back" onClick={onClose}>
                        {isEdit ? "Hủy" : "Đóng"}
                    </Button>,
                    isEdit && (
                        <Button
                            key="submit"
                            type="primary"
                            onClick={handleSubmit(onSubmit)}
                        >
                            Cập nhật
                        </Button>
                    ),
                ]}
            >
                <form className="user-info">
                    <div className="input-group">
                        <label className="label">ID</label>
                        <Controller
                            render={({ field }) => (
                                <Input {...field} disabled={true} />
                            )}
                            name="id"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.email?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Họ và tên</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="name"
                            control={control}
                            defaultValue=""
                            rules={{ required: true }}
                        />
                        <span className="error">{errors?.name?.message}</span>
                    </div>
                    <div className="input-group">
                        <label className="label">Email</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="email"
                            control={control}
                            rules={{ required: true }}
                        />
                        {/* <span className="error">{errors.password?.message}</span> */}
                    </div>
                    {userData?.roles[0].name === "student" && (
                        <div className="input-group">
                            <label className="label">Mã số sinh viên</label>
                            <Controller
                                render={({ field }) => (
                                    <Input disabled={!isEdit} {...field} />
                                )}
                                name="mssv"
                                control={control}
                                defaultValue=""
                                rules={{ required: true }}
                            />
                            {/* <span className="error">{errors.password?.message}</span> */}
                        </div>
                    )}
                    <div className="input-group">
                        <label className="label">Ngày sinh</label>
                        <Controller
                            render={({ field }) =>
                                !isEdit ? (
                                    <Input disabled={!isEdit} {...field} />
                                ) : (
                                    <DatePicker
                                        placeholder={"Ngày tháng"}
                                        style={{ width: "100%" }}
                                        onChange={onChangeDate}
                                        format="DD-MM-YYYY"
                                        showToday={false}
                                        value={
                                            date
                                                ? moment(date, "DD-MM-YYYY")
                                                : ""
                                        }
                                    />
                                )
                            }
                            name="dateOfBirth"
                            control={control}
                            defaultValue=""
                        />
                        {/* <span className="error">{errors.password?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Địa chỉ</label>
                        <Controller
                            render={({ field }) => (
                                <Input disabled={!isEdit} {...field} />
                            )}
                            name="address"
                            control={control}
                            defaultValue=""
                        />
                        {/* <span className="error">{errors.password?.message}</span> */}
                    </div>
                    <div className="input-group">
                        <label className="label">Chức danh</label>
                        <Controller
                            render={({ field }) => (
                                <Input
                                    disabled={true}
                                    {...field}
                                    value={userRole}
                                />
                            )}
                            name="role"
                            control={control}
                            defaultValue={userData?.roles[0].name}
                        />
                        {/* <span className="error">{errors.password?.message}</span> */}
                    </div>
                </form>
            </Modal>
        </div>
    );
}
export default ModalUserInformation;
