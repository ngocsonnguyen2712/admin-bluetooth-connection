import React, { useEffect, useState } from "react";
import {
    Table,
    Typography,
    message,
    Button,
    Input,
    Space,
} from "antd";
import {
    EyeOutlined,
    EditOutlined,
    DeleteOutlined,
    SearchOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";

import { deleteUser, getAllUsers } from "../../services/users";
import ModalUserInformation from "./ModalUserInfomation";
import ModalDelete from "../../commons/ModalDelete";
import ModalCreateNewUser from "./ModalCreateNewUser";

function Users() {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [visibleDelete, setVisibleDelete] = useState(false);
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [id, setId] = useState(null);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
    });
    const fetchData = async (pagination) => {
        setLoading(true);
        const res = await getAllUsers({
            page: pagination.current - 1,
            size: pagination.pageSize,
        });
        if (res) {
            setLoading(false);
            setData(res.content);
            setPagination({
                ...pagination,
                current: res.currentPage + 1,
                total: res.totalItems,
            });
        }
    };

    useEffect(() => {
        fetchData(pagination);
    }, []);

    const handleViewDetailUser = (record) => {
        setVisible(true);
        setId(record.id);
    };
    const handleEditUser = (record) => {
        setIsEdit(true);
        setVisible(true);
        setId(record.id);
    };
    const handleDeleteUser = async () => {
        const res = await deleteUser(id);
        if (res) {
            message.success("Xóa người dùng thành công");
            setVisibleDelete(false);
            fetchData(pagination);
        } else {
            message.success(
                "Có lỗi bất ngờ xảy ra. Vui lòng thử lại trong giây lát !"
            );
        }
    };

    const handleCloseModal = () => {
        setVisible(false);
        setIsEdit(false);
        setId(null);
    };

    const getColumnSearchProps = (dataIndex, placeholder) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    // ref={(node) => {
                    //     this.searchInput = node;
                    // }}
                    placeholder={`${placeholder}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ marginBottom: 8, display: "block", width: "100%" }}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() =>
                            handleSearch(selectedKeys, confirm, dataIndex)
                        }
                        icon={<SearchOutlined />}
                        size="small"
                        style={{ width: 100 }}
                    >
                        Tìm kiếm
                    </Button>
                    <Button
                        onClick={() => handleReset(clearFilters)}
                        size="small"
                        style={{ width: 80 }}
                    >
                        Hủy
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#1890ff" : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : "",
        render: (text) =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : ""}
                />
            ) : (
                text
            ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);
    };

    const handleReset = (clearFilters) => {
        clearFilters();
        setSearchText('');
    };

    const columns = [
        {
            title: "#",
            render: (value, row, index) => {
                return (
                    index + (pagination.current - 1) * pagination.pageSize + 1
                );
            },
            width: "5%",
        },
        {
            title: "ID",
            dataIndex: "id",
            width: "5%",
        },
        {
            title: "Họ và tên",
            dataIndex: "name",
            width: "15%",
            ...getColumnSearchProps("name", "Tên"),
        },
        {
            title: "Email",
            dataIndex: "email",
            width: "15%",
            ...getColumnSearchProps("email", "Email"),
        },
        {
            title: "Chức vụ",
            width: "10%",
            render: (_, record) => {
                const { roles } = record;
                return roles.map((item) => {
                    if(item.name === "teacher") {
                       return <Typography>Giảng viên</Typography>;
                    } else if(item.name === "student") {
                        return <Typography>Sinh viên</Typography>;
                    }
                    return <Typography>Admin</Typography>;
                });
            },
            onFilter: (value, record) => {
                const { roles } = record;
                const index = roles.findIndex((item) => item.name === value);
                return index > -1;
            },
            filters: [
                {
                    text: "Admin",
                    value: "admin",
                },
                {
                    text: "Giảng viên",
                    value: "teacher",
                },
                {
                    text: "Sinh viên",
                    value: "student",
                },
            ],
        },
        {
            title: () => (
                <Button
                    type="primary"
                    danger
                    onClick={() => setOpenModalAdd(true)}
                >
                    Tạo mới
                </Button>
            ),
            width: "10%",
            render: (_, record) => (
                <>
                    <span
                        style={{ marginRight: "30px", cursor: "pointer" }}
                        onClick={() => handleViewDetailUser(record)}
                    >
                        <EyeOutlined style={{ fontSize: 23 }} />
                    </span>
                    <span
                        style={{ marginRight: "30px", cursor: "pointer" }}
                        onClick={() => handleEditUser(record)}
                    >
                        <EditOutlined style={{ fontSize: 23 }} />
                    </span>
                    <span
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                            setId(record.id);
                            setVisibleDelete(true);
                        }}
                    >
                        <DeleteOutlined style={{ fontSize: 23 }} />
                    </span>
                </>
            ),
        },
    ];
    return (
        <div className="users-screen">
            <Table
                bordered
                dataSource={data}
                title={() => "Danh sách người dùng"}
                columns={columns}
                loading={loading}
                rowClassName="editable-row"
                pagination={{
                    ...pagination,
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "30", "50", "100"],
                }}
                onChange={fetchData}
            />
            <ModalUserInformation
                visible={visible}
                isEdit={isEdit}
                id={id}
                onClose={handleCloseModal}
                onSuccessful={() => fetchData(pagination)}
            />
            <ModalDelete
                visible={visibleDelete}
                handleDelete={handleDeleteUser}
                title="Xóa người dùng"
                handleClose={() => setVisibleDelete(false)}
            />
            <ModalCreateNewUser
                visible={openModalAdd}
                onSuccessful={() => fetchData(pagination)}
                handleClose={() => setOpenModalAdd(false)}
            />
        </div>
    );
}
export default Users;
