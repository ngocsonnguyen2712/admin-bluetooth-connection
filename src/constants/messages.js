const Messages = {
    NOT_VALID_EMAIL: 'Email không đúng định dạng',
    ERROR_REQUIRED: 'Trường bắt buộc phải nhập',
    ERROR_PASSWORD_MUST_MATCH: 'Mật khẩu xác nhận không khớp',
    ERROR_PASSWORD: 'Mật khẩu phải có ít nhất 6 ký tự',
    ACCEPT_RULES: 'Tôi đồng ý với điều khoản và điều kiện của ứng dụng',
    EMAIL_USED: 'Email đã được đăng ký cho một tài khoản khác.Vui lòng thử lại',
    WRONG_PASSWORD: 'Mật khẩu không chính xác. Vui lòng thử lại',
    EMAIL_NOT_EXIST: 'Tài khoản không tồn tại. Vui lòng thử lại',
    OLD_PASSWORD_DOES_NOT_MATCH: 'Mật khẩu không chính xác. Vui lòng thử lại',
    INVALID_DATE: 'Không đúng định dạng thời gian. Vui lòng thử lại',
    NOT_VALID_UUID: 'Mã tín hiệu không đúng định dạng',
};

export default Messages;
