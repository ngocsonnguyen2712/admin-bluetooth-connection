import axios from 'axios';
import _, { trim } from 'lodash';
import querystring from 'query-string';

import store from '../store';

let token = null;
const setToken = (_token) => { token = _token; };
const initToken = () => {
    if (localStorage.getItem('token')) {
        return setToken(localStorage.getItem('token'));
    }
    if (window.token) {
        return setToken(window.token);
    }
};
initToken();

function trimStrings(key, value) {
    if (typeof value === 'string') {
        return value.trim();
    }
    return value;
}

const http = axios.create({
    baseURL: `${process.env.REACT_APP_API_URL}`,
});


http.interceptors.request.use(
    (config) => {
        config.headers = config.headers || {};

        if ((Object.prototype.toString.call(config.data) === '[object Object]'
            || _.isArray(config.data)
            || _.isObject(config.data))
            && config.data.constructor && config.data.constructor.name !== 'FormData') {
            config.data = JSON.parse(JSON.stringify(config.data, trimStrings));
        }
        config.paramsSerializer = function (params) {
            const rs = {};
            Object.keys(params).forEach((key) => {
                if (typeof params[key] === 'string') {
                    rs[key] = trim(params[key]);
                } else {
                    rs[key] = params[key];
                }
            });
            return querystring.stringify(rs);
        };

        if (localStorage.getItem('token')) {
            config.headers["x-access-token"] = localStorage.getItem('token');
        }

        return config;
    },
    (error) => Promise.reject(error),
);

http.interceptors.response.use(
    (response) => {
        if (response.headers['content-disposition']) {
            return response;
        }
        if (response.data) {
            return response.data.results ? response.data.results : response.data;
        }
        return response;
    },
    async (error) => {
        const err = (error.response && error.response.data) || error;
        if (err.error === 'invalid_token') {
            // store.dispatch(signOut());
        }
        return Promise.reject(err);
    },
);

export { http }