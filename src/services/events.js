import { http } from "../helpers/http";
import { message } from "antd";
const endpoint = '/api/events';

const getAllEvents = async (params) => {
    const response = await http.get(endpoint, { params });
    return response;
}

const getDetailEvent = async (eventId) => {
    const response = await http.get(`${endpoint}/${eventId}`);
    return response;
}

const updateEvent = async (eventId, data) => {
    try {
        const response = await http.put(`${endpoint}/${eventId}`, data);
        return response;
    } catch (error) {
        message.error(error.message || 'Có lỗi xảy ra. Vui lòng thử lại sau giây lát !')
    }

}

const deleteEvent = async (eventId) => {
    const response = await http.delete(`${endpoint}/${eventId}`);
    return response;
}

const createEvent = async (data) => {
    try {
        const response = await http.post(endpoint, data);
        return response;
    } catch (error) {
        message.error(error.message || 'Có lỗi xảy ra. Vui lòng thử lại sau giây lát !')
    }
}

const getAllUserAttendanceAnEvent = async (eventId) => {
    const response = await http.get(`${endpoint}-users/${eventId}`);
    return response;
}

const downloadFile = async (eventId) => {
    const response = await http.get(`/api/download/${eventId}`, {
        responseType: "arraybuffer",
    });
    return response;
}

export {
    getAllEvents,
    getDetailEvent,
    updateEvent,
    deleteEvent,
    createEvent,
    getAllUserAttendanceAnEvent,
    downloadFile,
};