import { http } from "../helpers/http";
import { message } from "antd";
const endpoint = '/api/subjects';

const getAllSubjects = async (params) => {
    const response = await http.get(endpoint, { params });
    return response;
}

const getDetailSubject = async (subjectId) => {
    const response = await http.get(`${endpoint}/${subjectId}`);
    return response;
}

const updateSubject = async (subjectId, data) => {
    try {
        const response = await http.put(`${endpoint}/${subjectId}`, data);
        return response;
    } catch (error) {
        message.error(error.message || 'Có lỗi xảy ra. Vui lòng thử lại sau giây lát !')
    }

}

const deleteSubject = async (subjectId) => {
    const response = await http.delete(`${endpoint}/${subjectId}`);
    return response;
}

const createSubject = async (data) => {
    try {
        const response = await http.post(endpoint, data);
        return response;
    } catch (error) {
        message.error(error.message || 'Có lỗi xảy ra. Vui lòng thử lại sau giây lát !')
    }
}

export {
    getAllSubjects,
    getDetailSubject,
    updateSubject,
    deleteSubject,
    createSubject
}