import { http } from "../helpers/http";
import { message } from "antd";
const endpoint = "/api/users";

const getAllUsers = async (params) => {
    const response = await http.get(endpoint, { params });
    return response;
};

const getAllUsersRoleTeacher = async (params) => {
    const response = await http.get("/api/users-role-teacher");
    return response;
};

const getDetailUser = async (userId) => {
    const response = await http.get(`${endpoint}/${userId}`);
    return response;
};

const updateUser = async (userId, data) => {
    try {
        const response = await http.put(`${endpoint}/${userId}`, data);
        return response;
    } catch (error) {
        message.error(
            error.message || "Có lỗi xảy ra. Vui lòng thử lại sau giây lát !"
        );
    }
};

const deleteUser = async (userId) => {
    const response = await http.delete(`${endpoint}/${userId}`);
    return response;
};

const createUser = async (data) => {
    try {
        const response = await http.post(endpoint, data);
        return response;
    } catch (error) {
        message.error(
            error.message || "Có lỗi xảy ra. Vui lòng thử lại sau giây lát !"
        );
    }
};

const changePassword = async (data) => {
    try {
        const response = await http.post(`${endpoint}/change-password`, data);
        return response;
    } catch (error) {
        message.error(
            error.message || "Có lỗi xảy ra. Vui lòng thử lại sau giây lát !"
        );
    }
}

const forgotPassword = async (data) => {
    try {
        const response = await http.post(`${endpoint}/forgot-password`, data);
        return response;
    } catch (error) {
        message.error(
            error.message || "Có lỗi xảy ra. Vui lòng thử lại sau giây lát !"
        );
    }
}

export {
    getAllUsers,
    getDetailUser,
    updateUser,
    deleteUser,
    createUser,
    changePassword,
    forgotPassword,
    getAllUsersRoleTeacher,
};
