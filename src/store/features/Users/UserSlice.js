import { createSlice, createAsyncThunk } from "@reduxjs/toolkit"
import { http } from "../../../helpers/http"
import { message } from "antd"

export const loginUser = createAsyncThunk(
    "users/login",
    async ({ email, password }, thunkAPI) => {
        try {
            const response = await http.post('/api/auth/signin', { email, password, type: true })
            if (response.accessToken) {
                localStorage.setItem("token", response.accessToken);
                return response;
            } else {
                return thunkAPI.rejectWithValue(response);
            }
        } catch (e) {
            message.error(e.message);
            return thunkAPI.rejectWithValue(e)
        }
    }
)

export const fetchUserByToken = createAsyncThunk(
    'users/fetchUserByToken',
    async ({ token }, thunkAPI) => {
        try {
            const response = await fetch(
                "https://bluetooth-checking.herokuapp.com/api/user",
                {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "x-access-token": token,
                        "Content-Type": "application/json",
                    },
                }
            );
            let data = await response.json();
            if (response.status === 200) {
                return { ...data };
            } else {
                return thunkAPI.rejectWithValue(data);
            }
        } catch (e) {
            message.error(e.message);
            return thunkAPI.rejectWithValue(e);
        }
    }
);

export const userSlice = createSlice({
    name: "user",
    initialState: {
        id: null,
        username: "",
        email: "",
        role: "",
        isFetching: false,
        isSuccess: false,
        isError: false,
        errorMessage: "",
    },
    reducers: {
        clearState: (state) => {
            state.isError = false;
            state.isSuccess = false;
            state.isFetching = false;

            return state;
        },
    },
    extraReducers: {
        [loginUser.fulfilled]: (state, { payload }) => {
            state.id = payload.id;
            state.email = payload.email;
            state.username = payload.username;
            state.role = payload.roles[0];
            state.isFetching = false;
            state.isSuccess = true;
            return state;
        },
        [loginUser.rejected]: (state, { payload }) => {
            state.isFetching = false;
            state.isError = true;
            state.errorMessage = payload?.message;
        },
        [loginUser.pending]: (state) => {
            state.isFetching = true;
        },
        [fetchUserByToken.pending]: (state) => {
            state.isFetching = true;
        },
        [fetchUserByToken.fulfilled]: (state, { payload }) => {
            state.isFetching = false;
            state.isSuccess = true;
            state.id = payload.id;
            state.email = payload.email;
            state.username = payload.username;
            state.role = payload.roles[0];
        },
        [fetchUserByToken.rejected]: (state) => {
            state.isFetching = false;
            state.isError = true;
        },
    },
})
export const { clearState } = userSlice.actions;
export const userSelector = state => state.user