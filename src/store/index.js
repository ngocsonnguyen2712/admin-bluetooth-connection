import { configureStore } from '@reduxjs/toolkit'
import { userSlice  } from './features/Users/UserSlice';

export const store = configureStore({
    reducer: {
        user: userSlice.reducer
    },
})